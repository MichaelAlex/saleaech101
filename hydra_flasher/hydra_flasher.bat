@ECHO OFF

set port=
set firmware=

IF "%1"=="" (
    ECHO Command Line Arguments
    ECHO -p port_num : COM port number
    ECHO -f firmware : firmware file
    ECHO -h : help menu
    EXIT /b
)

:loop
IF NOT "%1"=="" (
    IF "%1"=="-h" (
        ECHO Command Line Arguments
	ECHO -p port_num : COM port number
	ECHO -f firmware : firmware file
	ECHO -h : help menu
        SHIFT
	EXIT /b
    )
    IF "%1"=="-p" (
        SET port=%2
        SHIFT
    )
    IF "%1"=="-f" (
        SET firmware=%2
        SHIFT
    )
    SHIFT
    GOTO :loop
)

IF "%port%"=="" (
    ECHO No COM port specified, use -p port_num
    EXIT /b
)
IF "%firmware%"=="" (
    ECHO No firmware specified, use -f firmware_file
    EXIT /b
)

ECHO Press CTRL+C to terminate process

ubeacon_console_app.exe -p %port% -b

if %ERRORLEVEL% == 0 (
  STMFlashLoader.exe -c --pn %port% --br 256000 -i STM32F4_12_1024K -e --all -d --a 0x08000000 --fn %firmware%
  ubeacon_console_app.exe -p %port% -r
)

@ECHO OFF

set port=
set firmware=