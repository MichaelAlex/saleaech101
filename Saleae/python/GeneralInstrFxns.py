  
import visa
import csv
import sys

filename = r'.\configs.csv' #The configs file where USB/GPIB addresses can be saved

def  create_visa_object(instr_addr): #replaces creation of gpib and usb object
# [gpib_obj, instrmodel] = GeneralInstrFxns.test_create_gpib_object(instr_addr)
#   Checks if the instrument at the GPIB/USB address is already connected.  If not connected yet
#   it creates a visa object, adjusts the input and output buffer
#   size, and opens communication
    if isinstance(instr_addr,int):  #if GPIB address
#    if str(instr_addr).isnumeric(): #if GPIB address
        visaAddress = 'GPIB0' + '::' + str(instr_addr) + '::INSTR'  #e.g., visaAddress = 'GPIB0::2::INSTR'        
    elif 'USB' in instr_addr: #USB
        visaAddress = instr_addr  #e.g., USB0::0x0957::0x4807::MY53300124::INSTR        
    else:
        sys.exit('Instrument address not recognized.  Enter GPIB address as number or valid USB address')   
    #Check if instrument at that address already connected
    rm = visa.ResourceManager()
    gpib_obj = rm.open_resource(visaAddress)
    #Change the input buffer size               
    gpib_obj.chunk_size = 132000 #gpib_obj.InputBufferSize = 132000 #Needed this value for multifunc (less for scope)               
    gpib_obj.timeout = 10000 #set timeout to 10 seconds    
   
    #Check if matches known gpib addresses
    modelinfo = gpib_obj.query('*IDN?')
    modelinfo_list = modelinfo.split(',')
    if len(modelinfo_list) > 1:
        instrmodel = modelinfo.split(',')[1]
    else: 
        instrmodel = modelinfo
    return gpib_obj, instrmodel

def read_from_config_csv(instr_in):
    try:
        with open(filename, 'r') as configfile:
            reader = csv.reader(configfile)
            for row in reader:
                if instr_in in row:
                    _address = str(row[1])
                    _address = _address.strip(' ')
                    try: #convert to int
                        _address = int(_address)
                    except:                         
                        try: #convert to float
                            _address = float(_address)
                        except: #if USB address or other keep as string    
                            pass
                    break
    except:
        _address = ''        
        sys.exit('Problem reading from the configs file - Correct the entry in ''Configs.csv'' for ' + instr_in)
    return _address          

    
def serial_switchinput(serialobj_ser, serial_parameter, serial_value):
    """ 
    Checks inputs for serial communication and switches to valid python input (parity, stopbits, bytesize)
    """
    if serial_parameter == 'parity':
        parity_list = 'none', 'even', 'odd', 'mark', 'space'
        if not serial_value.lower() in parity_list:
            serial_out = ''
            sys.exit('Incorrect value entered for' + serial_parameter + '.  Check configs.csv file. \n  Valid inputs: ' + str(parity_list))
        else:
            #serial_out = 'PARITY_' + serial_value.upper()
            serial_out = serial_value[0].upper()
    elif serial_parameter == 'stopbits':
        #stopbits_dict = {1: 'ONE', 1.5: 'ONE_POINT_FIVE', 2: 'TWO'}
        #stopbits_list = 1, 1.5, 2
        if not serial_value in serialobj_ser.STOPBITS:
            serial_out = -1
            sys.exit('Incorrect value entered for' + serial_parameter + '.  Check configs.csv file.\n  Valid inputs: 1, 1.5, 2')
        else:
            #serial_out = 'STOPBITS_' + stopbits_dict[serial_value]
            serial_out = serial_value
    elif serial_parameter == 'bytesize':
        #bytesize_dict = {5: 'FIVEBITS', 6: 'SIXBITS', 7: 'SEVENBITS', 8: 'EIGHTBITS'}
        #bytesize_list = 5, 6, 7, 8        
        if not serial_value in serialobj_ser.BYTESIZES:
            serial_out = -1
            sys.exit('Incorrect value entered for' + serial_parameter + '.  Check configs.csv file.\n  Valid inputs: ' + str(serialobj_ser.BYTESIZES))
        else:
            #serial_out= bytesize_dict[serial_value]
            serial_out = serial_value
    elif serial_parameter == 'baudrate':
        if not serial_value in serialobj_ser.BAUDRATES:
            serial_out = -1
            sys.exit('Incorrect value entered for' + serial_parameter + '.  Check configs.csv file.\n  Valid inputs: 5, 6, 7, 8')
        else:
            serial_out = serial_value      
    elif serial_parameter == 'controller_addr':
        serial_out = '0' + str(serial_value) 
    elif serial_parameter == 'terminator':
        if serial_value.upper() != 'CL':
            serial_out = ''
            sys.exit('Current functions assume carriage return and use ''readline'' to read Espec output. \n' +
                     'Set the terminator on the oven to ''CL'' and reflect in the configs.csv file')
        else:
            serial_out = serial_value.upper()
    
    return serial_out
        
    