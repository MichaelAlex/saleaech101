from Instruments import GeneralInstrFxns
try:
    import serial  #Used for Espec SU241 oven
except:
    pass
import time
import sys
import numpy as np
try:
    import minimalmodbus #used for Tenney
except:
    pass
import struct #used for Tenney


class Espec_SU241(object):
    """
    Espec SU-241 Class
    """
    def __init__(self):
        #Check pyserial version installed --> Need version 3.2.1 or greater
        if 'serial' not in sys.modules:
            sys.exit('"pyserial" package needs to be installed to use the Espec_SU241 module.  As an admin, from the command prompt type "pip install pyserial==3.2.1"')
        installed_ver = serial.VERSION
        if installed_ver != '3.2.1':
            sys.exit('Espec oven needs to use pyserial version 3.2.1 or greater.  From the command prompt type "pip install pyserial==3.2.1"')
        
        self.verbose = 0 #Default don't print statements
        
        #Setting default values        
        self.OVEN_TIMEOUT_SECS = 120*60
        self.NUM_STABLEREADINGS = 3 # 12 #number of readings to consider the temperature stable                                    
        self.READTEMP_SPACING = 5 #seconds between reading temperature during ramp                                                                                                                     
        
        self.ser = serial.Serial() 
        
        #Read values from configs.csv file and check for valid input values
        self.BAUDRATE_fromconfigs = GeneralInstrFxns.read_from_config_csv('ESPEC_OVEN_BAUDRATE')  
        self.ser.baudrate = GeneralInstrFxns.serial_switchinput(self.ser, 'baudrate', self.BAUDRATE_fromconfigs)
        self.PARITY_fromconfigs = GeneralInstrFxns.read_from_config_csv('ESPEC_OVEN_PARITY') 
        self.ser.parity = GeneralInstrFxns.serial_switchinput(self.ser, 'parity', self.PARITY_fromconfigs)
        self.STOPBITS_fromconfigs = GeneralInstrFxns.read_from_config_csv('ESPEC_OVEN_STOPBITS')  
        self.ser.stopbits = GeneralInstrFxns.serial_switchinput(self.ser, 'stopbits', self.STOPBITS_fromconfigs)
        self.BYTESIZE_fromconfigs = GeneralInstrFxns.read_from_config_csv('ESPEC_OVEN_DATALENGTH') 
        self.ser.bytesize = GeneralInstrFxns.serial_switchinput(self.ser, 'bytesize', self.BYTESIZE_fromconfigs)
        terminator = GeneralInstrFxns.read_from_config_csv('ESPEC_OVEN_TERMINATOR') 
        self.TERMINATOR = GeneralInstrFxns.serial_switchinput(self.ser, 'terminator', terminator)
        controller_addr = GeneralInstrFxns.read_from_config_csv('ESPEC_OVEN_CONTROLLER_ADDR') 
        self.CONTROLLER_ADDR = GeneralInstrFxns.serial_switchinput(self.ser, 'controller_addr', controller_addr)
        self.ser.timeout = 1 #seconds. 
        
    def openinstr(self, serialport = ''):
        """
        Opens communication with Espec oven \n
        INPUT(optional):  serialport:  e.g., 'COM3', Default: Uses value in configs.csv file for ESPEC_OVEN_SERIALPORT
        """
        if  not serialport:      
            self.serialport = GeneralInstrFxns.read_from_config_csv('ESPEC_OVEN_SERIALPORT')      
        else:
            self.serialport = serialport    
            
        self.ser.port = self.serialport #Set the serial port address
        
        if not self.ser.is_open: #open if not already opened
            self.ser.open()

    def _print_verbose(self,input):
        if self.verbose==1:
            print(input)
    
    def _decode_out(self):
        out = self.ser.readline() 
        out = out.decode('ascii')
        out = out.replace('\x00','')
        return out    
               
    def _send_check_command(self, cmd):
        """
        Sends a command and checks for a communication error
        """
        for i in range(0,10):
            try:
                self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', '.encode('ascii') + cmd.encode('ascii') + '\r\n'.encode('ascii'))
                out = self._decode_out()
                if out:
                    out = out.rstrip()
                    break
            except:
                pass
#                print('No communication with oven')
#                print(i)
            if i==9:
                self._print_verbose('Communication failed after 10 tries')
                out = 'error'            
        return out
            
    def close(self):
        """
        Closes communication with Espec oven
        """
        self.ser.close()
        
    def enable(self):
        """
        Enables oven temperature control
        
        """
        try:
            out = self._send_check_command('POWER, ON')
    #        self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', POWER, ON\r\n'.encode('ascii'))
    #        out = self._decode_out() 
        except:
            sys.exit("Oven Communication error during enable")
        return out 
        
    def disable(self):
        """
        Disables oven temperature control
        """
        try:
            out = self._send_check_command('POWER, OFF')
    #        self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', POWER, OFF\r\n'.encode('ascii'))
    #        out = self._decode_out()  
        except:
            sys.exit("Oven Communication error during enable")    
        return out

    def set_mode_Espec(self, ovenmode = ''):
        """
        Sets the oven temperature set point \n
        INPUT(optional):  
        ovenmode {str}: 'OFF','STANDBY','CONSTANT','RUN1' \n            
        OUTPUT:  
        out {str}:  Command sent status (If no input arguments supplied, returns the current mode.)  \n
        """
        if not ovenmode:
            #self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', MODE?\r\n'.encode('ascii'))
            #out = self._decode_out()  
            try:
                out = self._send_check_command('MODE?')
            except:
                print("Oven Communication error during query of 'set_mode_Espec'")
                out = 'Communication error'
        else:            
            ovenmode_list = 'OFF', 'STANDBY', 'CONSTANT', 'RUN1'
            if ovenmode.upper() in ovenmode_list:  
                ovenmode = ovenmode.upper()    
                #self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', MODE, '.encode('ascii') + str(ovenmode).encode('ascii') + '\r\n'.encode('ascii'))
                #out = self._decode_out()
                try:
                    out = self._send_check_command('MODE, ' + str(ovenmode))
                except:
                    print("Oven Communication error during 'set_mode_Espec'")
                    out = 'error'
                if ovenmode.upper() == 'CONSTANT':
                    #Set Auto refrigerator capacity control
                    #self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', SET, REF9\r\n'.encode('ascii'))
                    #out = self._decode_out()                    
                    try:
                        out = self._send_check_command('SET, REF9')
                    except:
                        sys.exit("Oven Communication error during 'set_mode_Espec'")
            else:
                out = "Invalid ovenmode input for 'set_mode_Espec'.  Valid inputs:  'off', 'standby', 'constant', 'run1'"
                sys.exit(out)
        return out 
            
    def set_temp(self, temperature_C):
        """
        Sets the oven temperature set point, also enables oven \n
        INPUT  temperature_C {float}: temperature, in Celsius
        """
        try:
            self.enable()
            self.set_mode_Espec('CONSTANT')
        
            temperature_C = round(float(temperature_C),1)    
            out = self._send_check_command('TEMP, S' + str(temperature_C))            
            #self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', TEMP, S'.encode('ascii') + str(temperature_C).encode('ascii') + '\r\n'.encode('ascii'))
            #out = self._decode_out() 
            msg = out.split(',')
            setpoint_read = msg[2]
            if setpoint_read.rstrip() != ('S' + str(temperature_C)):
                sys.exit('Communication error with oven:  Setpoint does not match desired value')
        except:
            sys.exit("Oven communication error during 'set_temp'")        
        return out

        
        
    def check_setpoint(self):
        """
        Check the oven temperature set point \n
        """
        try:
            out = self._send_check_command('TEMP?')
            out = out.split(',')
            setpoint = float(out[1])
        except:
            print("Oven Communication error during 'check_setpoint'")
            setpoint = -1000
        return setpoint
        
    def progstart(self):
        """
        Starts the program that's already manually loaded on the oven
        """
        try:
            out = self._send_check_command('MODE, RUN1')
        except:
            sys.exit("Oven Communication error during 'progstart'")
        return out
        
    def progstop(self):
        """
        Stops the program that's already manually loaded on the oven
        """
        try:
            out = self._send_check_command(', PRGM, END, STANDBY')
        except:
            sys.exit("Oven Communication error during 'progstop'")
        return out
        
    def progadvancestep_Espec(self):
        """
        Advances the program to the next step for Espec oven
        """
        try:
            out = self._send_check_command('PRGM, ADVANCE')
        except:
            sys.exit("Oven Communication error during 'progadvancestep_Espec'")
        return out            
        
    def get_temp(self):
        """
        Reads the current oven temperature \n
        USAGE:  measured_temp_C = get_temp()
        """
        try:
            msg = self._send_check_command('MON?')
#            self.ser.write(self.CONTROLLER_ADDR.encode('ascii') + ', MON?\r\n'.encode('ascii'))
#            msg = self._decode_out()
            msg = msg.split(',')
            meastemp_C = float(msg[0])
            mode = msg[2]
            alarms = float(msg[3])           
            #data = {'meastemp':meastemp_C,'mode':mode,'alarms':alarms}
                ##Usage for data:
                ##meas = get_temp()
                ##temp_C = data['meastemp']
                ##current_mode = data['mode']
                ##alarms = data['alarms']
        except:           
            print("Oven:  Communication error during 'get_temp' function")
            meastemp_C = -1000
        out = meastemp_C
        
        return out
        
    def stabilize_temperature(self, temp_C, soaktime_sec = 0, temp_tolerance_C = 2):
        """
        Stabilizes at a temperature defined by the tolerance band and then soaks for a given time
        USAGE:  temp_profile = [ovenhandle].stabilize_temperature(temp_C, soaktime_sec (opt), temp_tolerance_C(opt)) \n
        INPUTS: \n 
        temp_C (degrees C): Temperature SetPoint {float} \n
        soaktime_sec (secs): Desired soak time {float}, Default = 0 \n
        temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n
        OUTPUT: temp_profile {dictionary}:  dictionary of arrays ['timestamps','temp_meas'], \n
        temperature profile during ramp and soaking
        """
        data = -1
        
        try:
            temp_C = float(temp_C)
            soaktime_sec = float(soaktime_sec)
            temp_tolerance_C = float(temp_tolerance_C)
        except ValueError:
            sys.exit('OVEN stabilize_temperature: Unexpected input arguments: \n'
                         '        INPUTS: temp_C (degrees C): Temperature SetPoint {float} \n'
               'soaktime_sec (secs): Desired soak time {float}, Default = 0 \n'
                'temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n')
            
        timestamps = []
        temp_meas = []

        self.set_temp(temp_C)
        time.sleep(1)
        print('Going to ' + str(temp_C) + 'C...(will stabilize for ' + str(self.NUM_STABLEREADINGS*self.READTEMP_SPACING) + 
                        ' secs within +/- ' + str(temp_tolerance_C) + 'C)...')
        
        loop_numstablereadings = 0
        start_time = time.time()
        meastemp = self.get_temp()        
        while abs(temp_C-meastemp) > temp_tolerance_C or loop_numstablereadings < self.NUM_STABLEREADINGS:
            meastemp = self.get_temp()
            timestamps.append(time.time())
            temp_meas.append(meastemp)
            t_elapsed = start_time - time.time()
            if  t_elapsed > self.OVEN_TIMEOUT_SECS:
                print('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                self.set_temp(25) #go back to 25C
                sys.exit('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                break
            sys.stdout.write("\rTemperature: %4.1f C, Elapsed time:  %4.1f minutes" %(meastemp,(time.time()-start_time)/60))
            sys.stdout.flush()
            if abs(temp_C - meastemp) < temp_tolerance_C:
                loop_numstablereadings += 1
            else:
                loop_numstablereadings = 0
            time.sleep(self.READTEMP_SPACING)         
        self._print_verbose('\n---->Stabilized at temperature...')
        
        if soaktime_sec > 0: #Soak at temperature
            soak_start_time = time.time()
            elapsed_time = 0
            while elapsed_time < soaktime_sec:
                elapsed_time = time.time()-soak_start_time
                meastemp = self.get_temp()
                timestamps.append(time.time())
                temp_meas.append(meastemp)
                sys.stdout.write("\rSoaking for %4.0f secs....Elapsed time:  %4.0f secs" %(soaktime_sec,elapsed_time))
                sys.stdout.flush()
                time.sleep(self.READTEMP_SPACING)
            self._print_verbose('\n------>Done Soaking...')
        timestamps = np.asarray(timestamps)
        temp_meas = np.asarray(temp_meas)
        data = {'timestamps':timestamps,'temp_meas':temp_meas}                                           
        return data

        

            
class Tenney(object):
    """
    Tenney Benchtop Oven class
    """   
    def __init__(self):          
        if 'minimalmodbus' not in sys.modules:
            sys.exit('"minimalmodbus" package needs to be installed to use the Tenney module.  As an admin, from the command prompt type "pip install minimalmodbus"')
        
        self.verbose = 0 #Default don't print statements
        
        self.OVEN_TIMEOUT_SECS = 120*60 #seconds.  If the oven time to settle is longer than this, set this variable for a longer time
        self.NUM_STABLEREADINGS = 3 # 12 #number of readings to consider the temperature stable
        self.READTEMP_SPACING = 5 #seconds between reading temperature during ramp
        
        self.BAUDRATE_fromconfigs = GeneralInstrFxns.read_from_config_csv('TENNEY_OVEN_BAUDRATE') 
        self.PARITY_fromconfigs = GeneralInstrFxns.read_from_config_csv('TENNEY_OVEN_PARITY') 
        self.STOPBITS_fromconfigs = GeneralInstrFxns.read_from_config_csv('TENNEY_OVEN_STOPBITS')  
        self.BYTESIZE_fromconfigs = GeneralInstrFxns.read_from_config_csv('TENNEY_OVEN_DATALENGTH') 
        self.CONTROLLER_ADDR_fromconfigs  = GeneralInstrFxns.read_from_config_csv('TENNEY_OVEN_CONTROLLER_ADDR') 
        #self.terminator = GeneralInstrFxns.read_from_config_csv('TENNEY_OVEN_TERMINATOR') 
        
    def openinstr(self, serialport = ''):
        """Open communication with the instrument"""
        if  not serialport:      
            self.SERIALPORT = GeneralInstrFxns.read_from_config_csv('TENNEY_OVEN_SERIALPORT')      
        else:
            self.SERIALPORT = serialport           
        #Opens communication with oven
        self.instr = minimalmodbus.Instrument(self.SERIALPORT, self.CONTROLLER_ADDR_fromconfigs)        
        self.instr.MODE_RTU = 'rtu'      
        
        self.instr.serial.baudrate = self.BAUDRATE_fromconfigs
        self.instr.serial.bytesize = self.BYTESIZE_fromconfigs
        self.instr.serial.stopbits = self.STOPBITS_fromconfigs
        self.instr.serial.timeout = 0.05 # seconds

    def _print_verbose(self,input):
        if self.verbose==1:
            print(input)
    
    def enable(self):
        """
        Puts oven in 'Auto' mode (allows temperature control)
        """
        self.instr.write_registers(1880,list([10]))
        self._print_verbose('Oven enabled')
        
    def disable(self):
        """
        Disables oven compressors (Turns oven control 'Off')
        """
        self.instr.write_registers(1880,list([62]))
        self._print_verbose('Oven disabled')   
 
    def get_temp(self):
        """
        Reads oven current temperature
        Usage:  temp_degC = oven.get_temp()  
        """
        #global temperature_C
        temperature_register = self.instr.read_registers(360,2)
        mypack = struct.pack('<HH',temperature_register[0],temperature_register[1])
        f = struct.unpack('<f', mypack)
        temperature_C = (list(f)[0]-32) * 5/9
        
        return temperature_C
        
    def set_temp(self,temperature_C):
        """
        Sends the temperature set point to the oven
        Usage: oven.set_temp(temperature_C)
        """
        temp_f = temperature_C*9/5 + 32
        mypack1=struct.pack('<f',temp_f)
        tup = struct.unpack('<HH',mypack1)
        self.instr.write_registers(2160, list(tup))
        
    def stabilize_temperature(self, temp_C, soaktime_sec = 0, temp_tolerance_C = 1):
        """
        Stabilizes at a temperature defined by the tolerance band and then soaks for a given time
        USAGE:  temp_profile = [ovenhandle].stabilize_temperature(temp_C, soaktime_sec (opt), temp_tolerance_C(opt)) \n
        INPUTS: \n
        temp_C (degrees C): Temperature SetPoint {float} \n
        soaktime_sec (secs): Desired soak time {float}, Default = 0 \n
        temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n
        OUTPUT: temp_profile {dictionary}:  dictionary of arrays ['timestamps','temp_meas'], \n
        temperature profile during ramp and soaking
        """
        data = -1
        
        try:
            temp_C = float(temp_C)
            soaktime_sec = float(soaktime_sec)
            temp_tolerance_C = float(temp_tolerance_C)
            timestamps = []
            temp_meas = []
    
            self.set_temp(temp_C)
            time.sleep(1)
            print('Going to ' + str(temp_C) + 'C...(will stabilize for ' + str(self.NUM_STABLEREADINGS*self.READTEMP_SPACING) + 
                            ' secs within +/- ' + str(temp_tolerance_C) + 'C)...')
            
            loop_numstablereadings = 0
            start_time = time.time()
            meastemp = self.get_temp()        
            while abs(temp_C-meastemp) > temp_tolerance_C or loop_numstablereadings < self.NUM_STABLEREADINGS:
                meastemp = self.get_temp()
                timestamps.append(time.time())
                temp_meas.append(meastemp)
                t_elapsed = start_time - time.time()
                if  t_elapsed > self.OVEN_TIMEOUT_SECS:
                    print('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                    self.set_temp(25) #go back to 25C
                    sys.exit('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                    break
                sys.stdout.write("\rTemperature: %4.1f C, Elapsed time:  %4.1f minutes" %(meastemp,(time.time()-start_time)/60))
                sys.stdout.flush()
                if abs(temp_C - meastemp) < temp_tolerance_C:
                    loop_numstablereadings += 1
                else:
                    loop_numstablereadings = 0
                time.sleep(self.READTEMP_SPACING)         
            self._print_verbose('\n---->Stabilized at temperature...')
            
            if soaktime_sec > 0: #Soak at temperature
                soak_start_time = time.time()
                elapsed_time = 0
                while elapsed_time < soaktime_sec:
                    elapsed_time = time.time()-soak_start_time
                    meastemp = self.get_temp()
                    timestamps.append(time.time())
                    temp_meas.append(meastemp)
                    sys.stdout.write("\rSoaking for %4.0f secs....Elapsed time:  %4.0f secs" %(soaktime_sec,elapsed_time))
                    sys.stdout.flush()
                    time.sleep(self.READTEMP_SPACING)
                self._print_verbose('\n------>Done Soaking...')
            timestamps = np.asarray(timestamps)
            temp_meas = np.asarray(temp_meas)
            data = {'timestamps':timestamps,'temp_meas':temp_meas}                                           
            return data
        except ValueError:
            sys.exit('OVEN stabilize_temperature: Unexpected input arguments: \n'
                         '        INPUTS: temp_C (degrees C): Temperature SetPoint {float} \n'
               'soaktime_sec (secs): Desired soak time {float}, Default = 0 \n'
                'temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n')
            
            
class CSZ(object):
    """
    CSZ Benchtop Oven class
    """   
    def __init__(self):          
        if 'minimalmodbus' not in sys.modules:
            sys.exit('"minimalmodbus" package needs to be installed to use the CSZ module.  As an admin, from the command prompt type "pip install minimalmodbus"')
        
        self.verbose = 0 #Default don't print statements
        
        self.OVEN_TIMEOUT_SECS = 120*60 #seconds.  If the oven time to settle is longer than this, set this variable for a longer time
        self.NUM_STABLEREADINGS = 3 # 12 #number of readings to consider the temperature stable
        self.READTEMP_SPACING = 5 #seconds between reading temperature during ramp
        
        self.BAUDRATE_fromconfigs = GeneralInstrFxns.read_from_config_csv('CSZ_OVEN_BAUDRATE') 
        self.PARITY_fromconfigs = GeneralInstrFxns.read_from_config_csv('CSZ_OVEN_PARITY') 
        self.STOPBITS_fromconfigs = GeneralInstrFxns.read_from_config_csv('CSZ_OVEN_STOPBITS')  
        self.BYTESIZE_fromconfigs = GeneralInstrFxns.read_from_config_csv('CSZ_OVEN_DATALENGTH') 
        self.CONTROLLER_ADDR_fromconfigs  = GeneralInstrFxns.read_from_config_csv('CSZ_OVEN_CONTROLLER_ADDR') 
        self.terminator = GeneralInstrFxns.read_from_config_csv('CSZ_OVEN_TERMINATOR') 
        
    def openinstr(self, serialport = ''):
        """Open communication with the instrument"""
        if  not serialport:      
            self.SERIALPORT = GeneralInstrFxns.read_from_config_csv('CSZ_OVEN_SERIALPORT')      
        else:
            self.SERIALPORT = serialport           
        #Opens communication with oven
        self.instr = minimalmodbus.Instrument(self.SERIALPORT, self.CONTROLLER_ADDR_fromconfigs)        
        self.instr.MODE_RTU = 'rtu'      
        
        time.sleep(0.5)
        
        self.instr.serial.baudrate = self.BAUDRATE_fromconfigs
        self.instr.serial.bytesize = self.BYTESIZE_fromconfigs
        self.instr.serial.stopbits = self.STOPBITS_fromconfigs
        self.instr.serial.parity = GeneralInstrFxns.serial_switchinput(self.instr.serial, 'parity', self.PARITY_fromconfigs)
        self.instr.serial.timeout = 0.05 # seconds
        self.instr.serial.terminator = self.terminator
        self.instr.precalculate_read_size = True
    
        time.sleep(0.5)

    def _print_verbose(self,input):
        if self.verbose==1:
            print(input)
    
    def _readandiscard(self):
        #Discard a line - used for communication error handling
        try:
            self.instr.serial.readline()
        except:
            self.instr.serial.readline()
        
    def enable(self):
        """
        Puts oven in 'Auto' mode (allows temperature control)
        """
        for i in range(10):
            try:
                self.instr.write_registers(12,list([1]))
                self._readandiscard()
                self._print_verbose('Oven enabled')
                return
            except:
                #pass
                self._readandiscard()
            if i==9:
                self._print_verbose('Oven not enabled')
        
    def disable(self):
        """
        Disables oven compressors (Turns oven control 'Off')
        """
        for i in range(10):
            try:               
                self.instr.write_registers(12,list([0]))
                self._readandiscard() 
                self._print_verbose('Oven disabled')   
                return
            except:
                #pass
                self._readandiscard()
            if i==9:
                self._print_verbose('Oven not disabled')
 
    def get_temp(self):
        """
        Reads oven current temperature
        Usage:  temp_degC = oven.get_temp()  
        """
        for i in range(10):
            try:
                temperature_C = self.instr.read_register(35, numberOfDecimals=1,signed=True)
                self._readandiscard() 
                return temperature_C
            except: 
                self._readandiscard() 
            if i==9:
                print("Oven Communication error during 'get_temp'")
                temperature_C = -1000
                return temperature_C
#        meastemp = (obj.read_CSZ(35))/10;
#        mode=[]; alarms=[];
            #self._readandiscard()              
        
    def set_temp(self,temperature_C):
        """
        Sends the temperature set point to the oven
        Usage: oven.set_temp(temperature_C)
        """
#        temp_f = (temperature_C*9/5 + 32)*10
#        mypack1=struct.pack('<f',temp_f)
#        tup = struct.unpack('<HH',mypack1)
        try:
            temperature_C = round(float(temperature_C),1)
        except:
            sys.exit("Invalid argument entered for Oven 'set_temp': temperature_C {float}")
        for i in range(10):
            try:
                self.instr.write_register(36, temperature_C,numberOfDecimals=1,signed=True)
                return
            except:
                pass
            if i==9:
                print("Oven Communication error during 'set_temp'")
        
    def stabilize_temperature(self, temp_C, soaktime_sec = 0, temp_tolerance_C = 1):
        """
        Stabilizes at a temperature defined by the tolerance band and then soaks for a given time
        USAGE:  temp_profile = [ovenhandle].stabilize_temperature(temp_C, soaktime_sec (opt), temp_tolerance_C(opt)) \n
        INPUTS: \n
        temp_C (degrees C): Temperature SetPoint {float} \n
        soaktime_sec (secs): Desired soak time {float}, Default = 0 \n
        temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n
        OUTPUT: temp_profile {dictionary}:  dictionary of arrays ['timestamps','temp_meas'], \n
        temperature profile during ramp and soaking
        """
        data = -1
        
        try:
            temp_C = float(temp_C)
            soaktime_sec = float(soaktime_sec)
            temp_tolerance_C = float(temp_tolerance_C)
            timestamps = []
            temp_meas = []
    
            self.set_temp(temp_C)
            time.sleep(1)
            print('Going to ' + str(temp_C) + 'C...(will stabilize for ' + str(self.NUM_STABLEREADINGS*self.READTEMP_SPACING) + 
                            ' secs within +/- ' + str(temp_tolerance_C) + 'C)...')
            
            loop_numstablereadings = 0
            start_time = time.time()
            meastemp = self.get_temp()        
            while abs(temp_C-meastemp) > temp_tolerance_C or loop_numstablereadings < self.NUM_STABLEREADINGS:
                meastemp = self.get_temp()
                timestamps.append(time.time())
                temp_meas.append(meastemp)
                t_elapsed = start_time - time.time()
                if  t_elapsed > self.OVEN_TIMEOUT_SECS:
                    print('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                    self.set_temp(25) #go back to 25C
                    sys.exit('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                    break
                sys.stdout.write("\rTemperature: %4.1f C, Elapsed time:  %4.1f minutes" %(meastemp,(time.time()-start_time)/60))
                sys.stdout.flush()
                if abs(temp_C - meastemp) < temp_tolerance_C:
                    loop_numstablereadings += 1
                else:
                    loop_numstablereadings = 0
                time.sleep(self.READTEMP_SPACING)         
            self._print_verbose('\n---->Stabilized at temperature...')
            
            if soaktime_sec > 0: #Soak at temperature
                soak_start_time = time.time()
                elapsed_time = 0
                while elapsed_time < soaktime_sec:
                    elapsed_time = time.time()-soak_start_time
                    meastemp = self.get_temp()
                    timestamps.append(time.time())
                    temp_meas.append(meastemp)
                    sys.stdout.write("\rSoaking for %4.0f secs....Elapsed time:  %4.0f secs" %(soaktime_sec,elapsed_time))
                    sys.stdout.flush()
                    time.sleep(self.READTEMP_SPACING)
                self._print_verbose('\n------>Done Soaking...')
            timestamps = np.asarray(timestamps)
            temp_meas = np.asarray(temp_meas)
            data = {'timestamps':timestamps,'temp_meas':temp_meas}                                           
            return data
        except ValueError:
            sys.exit('OVEN stabilize_temperature: Unexpected input arguments: \n'
                         '        INPUTS: temp_C (degrees C): Temperature SetPoint {float} \n'
               'soaktime_sec (secs): Desired soak time {float}, Default = 0 \n'
                'temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n')

class Fluke1524(object):
    """
    Fluke 1524 Reference Thermometer
    """   
    def __init__(self):
                #Check pyserial version installed --> Need version 3.2.1 or greater
        if 'serial' not in sys.modules:
            sys.exit('"pyserial" package needs to be installed to use the Fluke module.  As an admin, from the command prompt type "pip install pyserial==3.2.1"')

        self.BAUDRATE     = 9600 
        self.PARITY       = serial.PARITY_NONE
        self.STOPBITS     = serial.STOPBITS_ONE
        self.BYTESIZE     = serial.EIGHTBITS 
        self.WRITETIMEOUT = 1
        self.TIMEOUT      = 1

    def openinstr(self, serialport = ''):
        self.ser = serial.Serial(
            port         = serialport,
            baudrate     = self.BAUDRATE,
            parity       = self.PARITY,
            stopbits     = self.STOPBITS,
            bytesize     = self.BYTESIZE,
            timeout      = self.TIMEOUT,
            write_timeout= self.WRITETIMEOUT    
        )     
    def closeinstr(self):
        self.ser.close()
    
    def readTemp(self, probenum):
        if(1 <= probenum <= 2):
            cmd = 'READ? %d\n' % (probenum) 
            self.ser.write(cmd.encode('ascii'))
            return float(self.ser.readline())
        else:
            sys.exit('ERROR: Invalid \'probenum\' provided. Valid probenum={1,2}. Example, fluke.readTemp(1).');

                     
class TestEquity(object):
    """
    TestEquity Oven (Model 107 tested)
        Assumes 'COM1' serial port.  Define the serialport in openinstr fxn if different
        Uses the oven settings for Tenney oven (baudrate, parity, stopbits, datalength, controller addr)
        Confirm oven settings:  
            Modbus Address: 1
            Baud rate:  9600
            Parity: None
            Display Units: C
            Modbus Word Order: High Low
            Data Map: 2 (compatible with F4 controllers)
    """   
    def __init__(self):          
        if 'minimalmodbus' not in sys.modules:
            sys.exit('"minimalmodbus" package needs to be installed to use the Tenney module.  As an admin, from the command prompt type "pip install minimalmodbus"')
        
        self.verbose = 0 #Default don't print statements
        
        self.OVEN_TIMEOUT_SECS = 120*60 #seconds.  If the oven time to settle is longer than this, set this variable for a longer time
        self.NUM_STABLEREADINGS = 3 # 12 #number of readings to consider the temperature stable
        self.READTEMP_SPACING = 5 #seconds between reading temperature during ramp
        
        self.BAUDRATE_fromconfigs = 9600
        self.PARITY_fromconfigs = 'none'
        self.STOPBITS_fromconfigs = 1
        self.BYTESIZE_fromconfigs = 8
        self.CONTROLLER_ADDR_fromconfigs  = 1
        
    def openinstr(self, serialport = ''):
        """Open communication with the instrument"""
        if  not serialport:      
            self.SERIALPORT = GeneralInstrFxns.read_from_config_csv('COM1')      
        else:
            self.SERIALPORT = serialport                     
        #Opens communication with oven
        self.instr = minimalmodbus.Instrument(self.SERIALPORT, self.CONTROLLER_ADDR_fromconfigs)        
        self.instr.MODE_RTU = 'rtu'      
        
        self.instr.serial.baudrate = self.BAUDRATE_fromconfigs
        self.instr.serial.bytesize = self.BYTESIZE_fromconfigs
        self.instr.serial.stopbits = self.STOPBITS_fromconfigs
        self.instr.serial.timeout = 0.05 # seconds

    def _print_verbose(self,input):
        if self.verbose==1:
            print(input)

    def enable(self):
        """
        If Temp switch is set to 'Event 1', will enable the oven temp control
        """
        for i in range(10):
            try:
                self.instr.write_registers(2000,list([1]))
                #self._readandiscard()
                self._print_verbose('Oven enabled')
                return
            except:
                pass
                #self._readandiscard()
            if i==9:
                self._print_verbose('Oven not enabled')
        
    def disable(self):
        """
        If Temp switch is set to 'Event 1', will disable the oven temp control (Disables oven compressors)
        """
        for i in range(10):
            try:               
                self.instr.write_registers(2000,list([0]))
                #self._readandiscard() 
                self._print_verbose('Oven disabled')   
                return
            except:
                pass
                #self._readandiscard()
            if i==9:
                self._print_verbose('Oven not disabled')
            
    def get_temp(self):
        """
        Reads oven current temperature
        Usage:  temp_degC = oven.get_temp()  
        """
        for i in range(10):
            try:
                temperature_C = self.instr.read_register(100, numberOfDecimals=1,signed=True)
                #self._readandiscard() 
                return temperature_C
            except: 
				pass
                #self._readandiscard() 
        if i==9:
            print("Oven Communication error during 'get_temp'")
            temperature_C = -1000
            return temperature_C
        
    def set_temp(self,temperature_C):
        """
        Sends the temperature set point to the oven
        Usage: oven.set_temp(temperature_C)
        """    
        try:
            temperature_C = round(float(temperature_C),1)
        except:
            sys.exit("Invalid argument entered for Oven 'set_temp': temperature_C {float}")
        for i in range(10):
            try:
                self.instr.write_register(300, temperature_C,numberOfDecimals=1,signed=True)
                return
            except:
                pass
            if i==9:
                print("Oven Communication error during 'set_temp'")
        
    def stabilize_temperature(self, temp_C, soaktime_sec = 0, temp_tolerance_C = 1):
        """
        Stabilizes at a temperature defined by the tolerance band and then soaks for a given time
        USAGE:  temp_profile = [ovenhandle].stabilize_temperature(temp_C, soaktime_sec (opt), temp_tolerance_C(opt)) \n
        INPUTS: \n
        temp_C (degrees C): Temperature SetPoint {float} \n
        soaktime_sec (secs): Desired soak time {float}, Default = 0 \n
        temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n
        OUTPUT: temp_profile {dictionary}:  dictionary of arrays ['timestamps','temp_meas'], \n
        temperature profile during ramp and soaking
        """
        data = -1
        
        try:
            temp_C = float(temp_C)
            soaktime_sec = float(soaktime_sec)
            temp_tolerance_C = float(temp_tolerance_C)
            timestamps = []
            temp_meas = []
    
            self.set_temp(temp_C)
            time.sleep(1)
            print('Going to ' + str(temp_C) + 'C...(will stabilize for ' + str(self.NUM_STABLEREADINGS*self.READTEMP_SPACING) + 
                            ' secs within +/- ' + str(temp_tolerance_C) + 'C)...')
            
            loop_numstablereadings = 0
            start_time = time.time()
            meastemp = self.get_temp()        
            while abs(temp_C-meastemp) > temp_tolerance_C or loop_numstablereadings < self.NUM_STABLEREADINGS:
                meastemp = self.get_temp()
                timestamps.append(time.time())
                temp_meas.append(meastemp)
                t_elapsed = start_time - time.time()
                if  t_elapsed > self.OVEN_TIMEOUT_SECS:
                    print('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                    self.set_temp(25) #go back to 25C
                    sys.exit('Oven timeout while trying to reach ' + str(temp_C) + ' after ' + str(self.OVEN_TIMEOUT_SECS/60) + ' minutes!')
                    break
                sys.stdout.write("\rTemperature: %4.1f C, Elapsed time:  %4.1f minutes" %(meastemp,(time.time()-start_time)/60))
                sys.stdout.flush()
                if abs(temp_C - meastemp) < temp_tolerance_C:
                    loop_numstablereadings += 1
                else:
                    loop_numstablereadings = 0
                time.sleep(self.READTEMP_SPACING)         
            self._print_verbose('\n---->Stabilized at temperature...')
            
            if soaktime_sec > 0: #Soak at temperature
                soak_start_time = time.time()
                elapsed_time = 0
                while elapsed_time < soaktime_sec:
                    elapsed_time = time.time()-soak_start_time
                    meastemp = self.get_temp()
                    timestamps.append(time.time())
                    temp_meas.append(meastemp)
                    sys.stdout.write("\rSoaking for %4.0f secs....Elapsed time:  %4.0f secs" %(soaktime_sec,elapsed_time))
                    sys.stdout.flush()
                    time.sleep(self.READTEMP_SPACING)
                self._print_verbose('\n------>Done Soaking...')
            timestamps = np.asarray(timestamps)
            temp_meas = np.asarray(temp_meas)
            data = {'timestamps':timestamps,'temp_meas':temp_meas}                                           
            return data
        except ValueError:
            sys.exit('OVEN stabilize_temperature: Unexpected input arguments: \n'
                         '        INPUTS: temp_C (degrees C): Temperature SetPoint {float} \n'
               'soaktime_sec (secs): Desired soak time {float}, Default = 0 \n'
                'temp_tolerance_C (degrees C): Desired oven tolerance band {float}, Default = 2 \n')