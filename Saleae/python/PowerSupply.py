import pyvisa
import sys 
from Instruments import GeneralInstrFxns


class Agilent_PS(object):
    """
    Agilent Power Supply E3647A dual output
    """
    def __init__(self):
        rm = pyvisa.ResourceManager()
        self._rm = rm
        self._dual_models = ['E3647A']
        self._triple_models = ['E3631A']
        self.verbose = 0 #Default don't print statements

    def openinstr(self, address=[], secondary = False):        
        try:
            if not address: #If nothing entered, use the default address
                if not secondary:
                    address = GeneralInstrFxns.read_from_config_csv('GPIB_POWERSUPPLY_ADDR_1')
                else:
                    address = GeneralInstrFxns.read_from_config_csv('GPIB_POWERSUPPLY_ADDR_2')
            [self.ps, self.instrmodel] = GeneralInstrFxns.create_visa_object(address) #Create GPIB object
        except(pyvisa.VisaIOError):
            sys.exit('Power Supply may not be properly connected or wrong address specified, check instrument address and physical connections')
                   
    def reset(self):
        """
        Reset Instrument \n
        """
        self.ps.write('*RST')
        
    def _print_verbose(self,input):
        if self.verbose==1:
            print(input)
        
    def on(self):
        """
        Toggle specified output on \n 
        """
        self.ps.write('OUTP ON')
                
            
    def off(self):
        """
        Toggle specified output off \n 
        """
        self.ps.write('OUTP OFF')


    def apply(self, voltage_V, current_A, output):
        """
        Select the output voltage and current in one command.\n
        voltage_V (volts): the output voltage {float} \n
        current_A (amps): the output current rating {float} \n
        output: The output number {float} \n 
        """
        if self.instrmodel in self._dual_models:
            if output in [1,2]:
                self.ps.write(':INST:SEL OUT'+str(output))
                try:
                    voltage_V = float(voltage_V)
                    current_A = float(current_A)
                    self.ps.write(str('APPL '+str(voltage_V)+', '+str(current_A)))
                except ValueError:                    
                    sys.exit("POWER SUPPLY: Unexpected argument(s) while using the apply function: voltage_V (volts): the output voltage {float} \n current_A (amps): the output current rating {float} ")
            else:                
                sys.exit("POWER SUPPLY:In using the apply function, please specify proper output (valid inputs are 1 or 2 as {float})")
        elif self.instrmodel in self._triple_models:
            triple_outputs = {1:'P6V', 2:'P25V', 3:'N25V'}
            if output not in triple_outputs:
                sys.exit("POWER SUPPLY:In using the apply function, specify valid output: 1, 2, or 3 as {float})")   
            try:
                voltage_V = float(voltage_V)
                current_A = float(current_A)                
                self.ps.write(str('APPL '+ triple_outputs[output] + ', ' + str(voltage_V) + ', ' + str(current_A)))
                if output == 3 and voltage_V >0:
                    sys.exit('Third output (-25V) only accepts negative voltage settings')
            except ValueError:                  
                sys.exit('POWER SUPPLY:Missing or Unexpected argument(s) while using the apply function: voltage_V (volts): the output voltage {float} \n current_A (amps): the output current rating {float}')
        else:           
            sys.exit('Unknown power supply model, please append current model to either the "self._dual_models" or the "self._triple_models" list , based on number of outputs, at the top of PowerSupply.py script in the Instruments directory')
            
    def set_voltage(self, voltage_V, output):
        """
        Select the output voltage \n
        INPUTS:  \n
        voltage_V (volts): the output voltage {float} \n
        output: The output number {float} \n 
        """
        if self.instrmodel in self._dual_models:
            if output in [1,2]:
                self.ps.write(':INST:SEL OUT' + str(output))
                try:
                    voltage_V = float(voltage_V)
                    self.ps.write(str(':VOLT ' + str(voltage_V)))
                except ValueError:
                    sys.exit('POWER SUPPLY:Missing or Unexpected argument(s) while using the set_voltage function: voltage_V (volts): the output voltage {float}')
            else:
                sys.exit("POWER SUPPLY:In using the set_voltage function, please specify proper output (valid inputs are 1 or 2 as {float})")
        elif self.instrmodel in self._triple_models:
            triple_outputs = {1:'P6V', 2:'P25V', 3:'N25V'}
            if output not in triple_outputs:
                sys.exit('POWER SUPPLY:Using the measure_current function, specify correct output (valid options are 1, 2, or 3 corresponding to "+6V", "+25V", or "-25V" respectively)')   
            try:
                voltage_V = float(voltage_V)
                self.ps.write(':INST:SEL ' + triple_outputs[output])
                self.ps.write(str('VOLT ' + str(voltage_V)))
                if output == 3 and voltage_V >0:
                    sys.exit('Third output (-25V) only accepts negative voltage settings')
            except ValueError:
                sys.exit('POWER SUPPLY:Unexpected argument(s) while using the set_voltage function: refer to documentation for proper usage')            
        else:            
            sys.exit('Unknown power supply model, please append current model to either the "self._dual_models" or the "self._triple_models" list at the top of PowerSupply.py script in the Instruments directory')
            
    def set_current(self, current_A, output):
        """
        Select the output current \n
        INPUTS:  \n
        current_A (Amps): the output current {float} \n
        output: The output number {float} \n 
        """
        if self.instrmodel in self._dual_models:           
            if output in [1,2]:
                self.ps.write(':INST:SEL OUT' + str(output))                
                try:
                    current_A = float(current_A)
                    self.ps.write(str('CURR ' + str(current_A)))
                except ValueError:                   
                    sys.exit('POWER SUPPLY:Missing or Unexpected argument(s) while using the set_current function: current_A (amps): the output current {float}')  
            else:                
                sys.exit("POWER SUPPLY:In using the set_current function, please specify proper output (valid inputs are 1 or 2 as {float})")  
                    
        elif self.instrmodel in self._triple_models:
            triple_outputs = {1:'P6V', 2:'P25V', 3:'N25V'}
            if output not in triple_outputs:
                sys.exit('POWER SUPPLY:Using the measure_current function, specify correct output (valid options are 1, 2, or 3 corresponding to "+6V", "+25V", or "-25V" respectively)')   
            try:
                current_A = float(current_A)
                self.ps.write(':INST:SEL ' + triple_outputs[output])
                self.ps.write(str('CURR ' + str(current_A)))
            except ValueError:                
                sys.exit('POWER SUPPLY:Missing or Unexpected argument(s) while using the set_current function: current_A (amps): the output current {float}')                       
        else:            
            sys.exit('Unknown power supply model, please append current model to either the "self._dual_models" or the "self._triple_models" list at the top of PowerSupply.py script in the Instruments directory')
            
    def measure_voltage(self, output):
        """
        Returns float value of voltage (Volts) at the sense terminals of the power supply \n
        output: The output number {float} \n  
        """
        if self.instrmodel in self._dual_models:
            try:
                if output in [1,2]:
                    self.ps.write(':INST:SEL OUT'+str(output))
                    out = float(self.ps.query(':MEAS:VOLT?'))
                else:
                    out = -1
                    sys.exit('PS: In the measure_voltage function, specify valid output {float}: 1|2')
                return out           
            except(pyvisa.VisaIOError):
                print('Power Supply may not be properly connected, check physical connections')

        elif self.instrmodel in self._triple_models:
            triple_outputs = {1:'P6V', 2:'P25V', 3:'N25V'}
            if output not in triple_outputs:
                sys.exit('POWER SUPPLY:Using the measure_current function, specify correct output (valid options are 1, 2, or 3 corresponding to "+6V", "+25V", or "-25V" respectively)')   
            try:               
                self.ps.write(':INST:SEL ' + triple_outputs[output])
                out = float(self.ps.query(':MEAS:VOLT?'))    
                return out                
            except(pyvisa.VisaIOError):
                print('Power Supply may not be properly connected, check physical connections')
        else:           
            sys.exit('Unknown power supply model, please append current model to either the "self._dual_models" or the "self._triple_models" list at the top of PowerSupply.py script in the Instruments directory')
            
    def measure_current(self, output):
        """
        Returns float value of current (amps) across the current sense resistor inside the power supply. \n
        output: The output number {float} \n  
        """
        if self.instrmodel in self._dual_models:
            try:
                if output in [1,2]:
                    self.ps.write(':INST:SEL OUT'+str(output))
                    out = float(self.ps.query(':MEAS:CURR?'))
                else:
                    out = -1
                    sys.exit('POWER SUPPLY:In using the measure_current function, specify valid output {float}: 1|2')
                return out            
            except(pyvisa.VisaIOError):
                print('Power Supply may not be properly connected, check physical connections')        

        elif self.instrmodel in self._triple_models:
            triple_outputs = {1:'P6V', 2:'P25V', 3:'N25V'}
            if output not in triple_outputs:
                sys.exit('POWER SUPPLY:Using the measure_current function, specify correct output (valid options are 1, 2, or 3 corresponding to "+6V", "+25V", or "-25V" respectively)')                         
            try:
                self.ps.write(':INST:SEL ' + triple_outputs[output])
                out = float(self.ps.query(':MEAS:CURR?'))
                return out                
            except(pyvisa.VisaIOError):
                print('Power Supply may not be properly connected, check physical connections')
        else:
            
            sys.exit('Unknown power supply model, please append current model to either the "self._dual_models" or the "self._triple_models" list at the top of PowerSupply.py script in the Instruments directory')
            
    def set_overvoltage_limit(self, volt_limit_V, output): #(dual only)
        """
        Set overvoltage protection level (OVP)  \n
        INPUTS:  \n
        volt_limit_V (Volts): overvoltage protection trip level {float} \n
        output: The output number {float} \n 
        """
        if self.instrmodel in self._dual_models:
            if output in [1,2]:
                try:
                    volt_limit_V = float(volt_limit_V)
                    self.ps.write(':INST:SEL OUT'+str(output))
                    self.ps.write('VOLTage:PROTection:STATe 1')
                    self.ps.write('VOLTage:PROTection '+str(volt_limit_V))
                except ValueError:                    
                    sys.exit('POWER SUPPLY:Missing or Unexpected argument(s) using the set_overvoltage_limit function: volt_limit_V (Volts): overvoltage protection trip level {float}')
            else:                
                sys.exit("POWER SUPPLY:Using the set_overvoltage_limit function, specify proper output (valid options are 1 or 2 as {float})")

        elif self.instrmodel in self._triple_models:
            sys.exit('POWER SUPPLY:set_overvoltage_limit feature not available for this model')
        else:            
            sys.exit('Unknown power supply model, please append current model to either the "self._dual_models" or the "self._triple_models" list at the top of PowerSupply.py script in the Instruments directory')
            
    def reset_output_protection(self, output): #(dual only)
        """
        Reset the output protection settings for specified output \n
        output: The output number {float} \n  
        """
        if self.instrmodel in self._dual_models:
            if output in [1,2]:
                self.ps.write(':INST:SEL OUT' + str(output))
                self.ps.write('VOLTage:PROTection:CLEar')
            else:                
                sys.exit('POWER SUPPLY:Missing or Unexpected argument(s) using the reset_output_protection function: Specify correct output (valid options are 1 or 2)')        
        elif self.instrmodel in self._triple_models:            
            sys.exit('POWER SUPPLY:reset_output_protection feature not available for this model')
        else:            
            sys.exit('Unknown power supply model, please append current model to either the "self._dual_models" or the "self._triple_models" list at the top of PowerSupply.py script in the Instruments directory')
