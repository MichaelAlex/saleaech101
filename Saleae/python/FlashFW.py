# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 15:23:46 2019

@author: malex
"""

import os
import subprocess
Dict = {1.6: 'dut_voltage/hydra_final_ch101_v99.28.1.0.47.0.9_1v6.bin', 
        1.8: 'dut_voltage/hydra_final_ch101_v99.28.1.0.47.0.9_1v8.bin', 
        2.0: 'dut_voltage/hydra_final_ch101_v99.28.1.0.47.0.9_2v0.bin'}

hydra_com_port = 4

def switchvoltage(hydra_com_port,level):
    print("flashing firmware to set the voltage to " + str(level))
    flasher_path = r'C:\Users\malex\Documents\hydra_flasher'
    os.chdir(flasher_path)
    p = subprocess.Popen("hydra_flasher.bat " + "-p " + str(hydra_com_port) + " -f " + Dict[level], shell=True, stdout=subprocess.PIPE)
    for line in p.stdout:
        print(line)
        
for level in Dict:
    switchvoltage(hydra_com_port,level)
    
