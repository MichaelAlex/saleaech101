To run the command line script hydraterm.bat v0.9

usage: HydraTerm [-h] [-b] [-i I] -p P [-t] [-v] [-f F]

optional arguments:
  -h, --help  Show this help message and exit
  -b          Board info
  -i I        Test ID
  -p P        COM port no.
  -t          Perform self-test
  -v          Show program's version number and exit
  -f          Use fixed frequency (Hz)

example: hydraterm -p 17
